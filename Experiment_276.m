% Title: Reproducibility of results of strain rate friction experiments on 
% a calcite sample
% Content: Measurements of the HighSTEPS Apparatus on strain rate friction experiment
% Experiment conditions: normal stress 95 MPa, Confining pressure 50 MPa,
% stiffness k = 0.002017 1/um (Experiment number 276)
% Author: Joana-Sophia Levkov
% EPFL
% Created: 15/10/2021
% Last upgrade: 25/10/2021

clear variables %clear the workspace variables
close all %close alla figures
clc %clear the command window

%% STEP 0: import data
data=readtable('Experiment_276.csv');

%initialize the force and displacement vectors:
VerticalPistonLoad_kN_ = data.VerticalPistonLoad_kN_; %en kN
HorizontalPistonLoad_kN_ = data.HorizontalPistonLoad_kN_; %en kN
ConfiningPressure_chamber__MPa_ = data.ConfiningPressure_chamber__MPa_; %here apply the chamber confining pressure [MPa]
Time_sec = cumsum(abs(data.TimeDiff)); %measured time in sec
VerticalPiston_mm_= data.VerticalPiston_mm_; %vertical position of piston
HorizontalPiston_mm_= data.HorizontalPiston_mm_; %horizontal position of pistion

%% STEP 1: plot Vertical load to identify point at which load starts to increase
figure();
plot(VerticalPistonLoad_kN_);
title('Vertical load [kN]');
xlabel('steps');
ylabel('load [kN]');

%find point at which vertical force starts to increase
index_start= 1.733 * 10^5; 

%find point at which vertical force is drastically decreasing
index_end = 3.366 * 10^5;

%vertical load at this point:
VerticalPistonLoad_kN_start = VerticalPistonLoad_kN_(index_start);

%reset vertical load 
VerticalPistonLoad_kN_reseted = VerticalPistonLoad_kN_-VerticalPistonLoad_kN_start;

%% STEP 3: Compute slip = amount of displacement in the shear direction (here vertical)

%find position at which first contact startet:
VerticalPiston_mm_first = VerticalPiston_mm_(index_start);

%compute the vertical slip of the sample (the y-axis points upwards)
Vertical_Slip = abs(VerticalPiston_mm_-VerticalPiston_mm_first);

%% STEP 4: Filter forces

%Filter the horizontal piston load:
HorizontalPistonLoad_kN_filter = sgolayfilt(HorizontalPistonLoad_kN_,3,51);

%Filter confined pressure 
ConfiningPressure_chamber__MPa_filter = sgolayfilt(ConfiningPressure_chamber__MPa_,3,51);

%% STEP 5: Compute stresses 

%first the contact area needs to be computed. In this experiment the
%contact area is 2x3.4 cm:
A_cm = 2*3.4; %in cm^2
A_mm = 2*10*3.4*10; %in mm^

%compute shear stress [MPa = N/mm^2] using the unfiltered vertical piston
%load:
shear_stress_MPa = (VerticalPistonLoad_kN_reseted/A_mm)*1.0e03;

%Correction factor for the normal stress as it is given in paper HighSTEPS:
%A High Strain Temperature Pressure and Speed Apparatus to Study Earthquake
%Mechanics by M.Violay:
correction_factor = 2.023;

%initialize the vector containing the normal stress [MPa = N/mm^2]
normal_stress_MPa = [];

%compute the normal stresses using the formula (1) given in paper HighSTEPS:
%A High Strain Temperature Pressure and Speed Apparatus to Study Earthquake
%Mechanics by M.Violay:
for i=1:length(HorizontalPistonLoad_kN_filter)
    normal_stress_MPa(i) = ((HorizontalPistonLoad_kN_filter(i) - correction_factor*ConfiningPressure_chamber__MPa_filter(i))*1.0e3/A_mm);
end
    


% initialize the vector containing the shear stress [MPa = N/mm^2]
frict_coeff = [];

%compute the friction coefficient by shear_stress/normal_stress:
for i=1:length(shear_stress_MPa)
    frict_coeff(i) = shear_stress_MPa(i)/normal_stress_MPa(i);
end

%reduce vectors from starting to end point:
Time_sec_reduced = Time_sec(index_start:index_end);
Vertical_Slip_reduced =Vertical_Slip(index_start:index_end);
frict_coeff_reduced = frict_coeff(index_start:index_end);
normal_stress_MPa_reduced = normal_stress_MPa(index_start:index_end);




%% STEP 6: Plot friction coefficient vs vertical slip:

figure();
plot(Vertical_Slip_reduced,frict_coeff_reduced);
title('Friction coefficient vs. vertical slip');
xlabel('vertical slip [mm]');
ylabel('friction coefficient [-]');


%% STEP 7: Prepare vectors for the RSfit300 code:

%rename the variables in order to match with the variables name of the 
%RSFit300 tool and
%change unitiy of vertical piston position: 

load_point_displacement = Vertical_Slip_reduced*1000; %multiply by 1000 to convert from mm to micrometer
friction_data = frict_coeff_reduced;
time_data = Time_sec_reduced;
normal_stress = normal_stress_MPa_reduced;


