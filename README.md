# Project: strain-rate-friction experiment on calcite

## Description
This Matlab file imports the measurements of a strain-rate-friction experiment performed on a rectangular calcite sample with the HighSTEPS apparatus. Furthermore, it extracts the needed variables in order to plot the friction coefficient versus the vertical slip.  

## Author
The code was created by Joana-Sophia Levkov. If there are any question regarding the code, please contact her by mail: joana-sophia.levkov@epfl.ch

## Getting started
### Dependencies
This code requires Matlab (version 9.4, R2018a) .
In order to execute this matlab file, the "signal processing toolbox" has to be installed. If it is not yet installed, go to matlab, select the "APPS" tab, push the "get more APPS" button and search in the explorer for "signal processing toolbox". If it is already installed you can proceed to step 0. If not, please install it. It enables you to use the function "sgolayfit(x,yz)" used further in the code.

## Step 0: Import data

First of all, the measurements of the experiment have to be loaded. The measurement data are saved in a ".csv" file (here Experiment_276.csv) and should be in the same folder than the matlab script (here Experiment_276.m). For this sake, the exact name of the csv file has to be entered in 16 in the matlab script.

In the next lines, variables are extracted which will be used to plot the friction coefficient and the vertical slip.

## Step 1: Plot vertical load

Plot the vertical piston load in order to identify the moment at which the load starts drastically to increase. Locate the loading start point directly on the figure using the *data cursor*. Save this loading start point under the variable "index_start".
Also determine the point where the load decreases drastically and save it under the variable "index_end".
The vertical load vector must be reduced by the initial load at the "index_start" point.

## Step 2: Compute vertical slip

In order to compute the vertical slip, first the initial position at the first contact between the piston and the sample has to be found. To do so use the vector "VerticalPiston_mm_" and the "index_start" variable. 
Finally the slip is the absolute difference of the current piston position and the initial one. 

## Step 3: Filter forces

As the horizontal force and the confining pressure vectors are noisy, one applies a Savitzky-Golay finite impulse response (FIR) smoothing filter of polynomial order `3` and frame length `51` to the data. The order and frame length were determined after many trials and seem to give a valid filter. 

## Step 4: Compute stresses

In this step the shear stress and the normal stress is computed. In order to do so, first the contact area needs to be calculated, then the shear stress is simply determined by dividing the vertical load by the contact area. The normal stress is calculated using the equation (1) given in the paper HighSTEPS: A High Strain Temperature Pressure and Speed Apparatus to Study Earthquake Mechanics by M.Violay taking account of a correction factor and the confining pressure in the chamber.
Finally the friction coefficient is calculated by dividing the shear stress by the normal stress. It is important that both stresses are specified in the unit [MPa].

## Step 5: Plot friction coefficient vs vertical slip:

In this step, the previous calculated friction coefficient vector is plotted versus the vertical slip computed at step 2. This graph, shows the dependency of the friction coefficient on the vertical slip. In a next step, the state-rate friction law parameters are searched using numerical fitting algorithms.
The resulting plot should be resemble to the following one:
![ResultPlot](/uploads/9f4b810a2f5d9f5c67fed09e01eb62b0/ResultPlot.png)
